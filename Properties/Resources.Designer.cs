﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ElasticSearchTestConnector.Properties {
    using System;
    
    
    /// <summary>
    ///   Eine stark typisierte Ressourcenklasse zum Suchen von lokalisierten Zeichenfolgen usw.
    /// </summary>
    // Diese Klasse wurde von der StronglyTypedResourceBuilder automatisch generiert
    // -Klasse über ein Tool wie ResGen oder Visual Studio automatisch generiert.
    // Um einen Member hinzuzufügen oder zu entfernen, bearbeiten Sie die .ResX-Datei und führen dann ResGen
    // mit der /str-Option erneut aus, oder Sie erstellen Ihr VS-Projekt neu.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Gibt die zwischengespeicherte ResourceManager-Instanz zurück, die von dieser Klasse verwendet wird.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ElasticSearchTestConnector.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Überschreibt die CurrentUICulture-Eigenschaft des aktuellen Threads für alle
        ///   Ressourcenzuordnungen, die diese stark typisierte Ressourcenklasse verwenden.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Background_Server ähnelt.
        /// </summary>
        internal static string NotifyChange_BackgroundServer {
            get {
                return ResourceManager.GetString("NotifyChange_BackgroundServer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Button_Query ähnelt.
        /// </summary>
        internal static string NotifyChange_ButtonQuery {
            get {
                return ResourceManager.GetString("NotifyChange_ButtonQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die EnabledButtonOrderFirst ähnelt.
        /// </summary>
        internal static string NotifyChange_EnabledButtonOrderFirst {
            get {
                return ResourceManager.GetString("NotifyChange_EnabledButtonOrderFirst", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die EnabledButtonOrderLast ähnelt.
        /// </summary>
        internal static string NotifyChange_EnabledButtonOrderLast {
            get {
                return ResourceManager.GetString("NotifyChange_EnabledButtonOrderLast", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die EnabledButtonOrderNext ähnelt.
        /// </summary>
        internal static string NotifyChange_EnabledButtonOrderNext {
            get {
                return ResourceManager.GetString("NotifyChange_EnabledButtonOrderNext", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die EnabledButtonOrderPrevious ähnelt.
        /// </summary>
        internal static string NotifyChange_EnabledButtonOrderPrevious {
            get {
                return ResourceManager.GetString("NotifyChange_EnabledButtonOrderPrevious", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Index ähnelt.
        /// </summary>
        internal static string NotifyChange_Index {
            get {
                return ResourceManager.GetString("NotifyChange_Index", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die ItemList ähnelt.
        /// </summary>
        internal static string NotifyChange_ItemList {
            get {
                return ResourceManager.GetString("NotifyChange_ItemList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Label_Index ähnelt.
        /// </summary>
        internal static string NotifyChange_LabelIndex {
            get {
                return ResourceManager.GetString("NotifyChange_LabelIndex", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Label_Navigation ähnelt.
        /// </summary>
        internal static string NotifyChange_LabelNavigation {
            get {
                return ResourceManager.GetString("NotifyChange_LabelNavigation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Label_Port ähnelt.
        /// </summary>
        internal static string NotifyChange_LabelPort {
            get {
                return ResourceManager.GetString("NotifyChange_LabelPort", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Label_Query ähnelt.
        /// </summary>
        internal static string NotifyChange_LabelQuery {
            get {
                return ResourceManager.GetString("NotifyChange_LabelQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Label_Server ähnelt.
        /// </summary>
        internal static string NotifyChange_LabelServer {
            get {
                return ResourceManager.GetString("NotifyChange_LabelServer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Label_Type ähnelt.
        /// </summary>
        internal static string NotifyChange_LabelType {
            get {
                return ResourceManager.GetString("NotifyChange_LabelType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die OpacityButtonFirst ähnelt.
        /// </summary>
        internal static string NotifyChange_OpacityButtonFirst {
            get {
                return ResourceManager.GetString("NotifyChange_OpacityButtonFirst", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die OpacityButtonLast ähnelt.
        /// </summary>
        internal static string NotifyChange_OpacityButtonLast {
            get {
                return ResourceManager.GetString("NotifyChange_OpacityButtonLast", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die OpacityButtonNext ähnelt.
        /// </summary>
        internal static string NotifyChange_OpacityButtonNext {
            get {
                return ResourceManager.GetString("NotifyChange_OpacityButtonNext", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die OpacityButtonPrevious ähnelt.
        /// </summary>
        internal static string NotifyChange_OpacityButtonPrevious {
            get {
                return ResourceManager.GetString("NotifyChange_OpacityButtonPrevious", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Page ähnelt.
        /// </summary>
        internal static string NotifyChange_Page {
            get {
                return ResourceManager.GetString("NotifyChange_Page", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die PageCount ähnelt.
        /// </summary>
        internal static string NotifyChange_PageCount {
            get {
                return ResourceManager.GetString("NotifyChange_PageCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Port ähnelt.
        /// </summary>
        internal static string NotifyChange_Port {
            get {
                return ResourceManager.GetString("NotifyChange_Port", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Query ähnelt.
        /// </summary>
        internal static string NotifyChange_Query {
            get {
                return ResourceManager.GetString("NotifyChange_Query", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Server ähnelt.
        /// </summary>
        internal static string NotifyChange_Server {
            get {
                return ResourceManager.GetString("NotifyChange_Server", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Type ähnelt.
        /// </summary>
        internal static string NotifyChange_Type {
            get {
                return ResourceManager.GetString("NotifyChange_Type", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap pulsante_01_architetto_f_01 {
            get {
                object obj = ResourceManager.GetObject("pulsante_01_architetto_f_01", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap pulsante_01_architetto_f_01_First1 {
            get {
                object obj = ResourceManager.GetObject("pulsante_01_architetto_f_01_First1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap pulsante_02_architetto_f_01 {
            get {
                object obj = ResourceManager.GetObject("pulsante_02_architetto_f_01", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Ressource vom Typ System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap pulsante_02_architetto_f_01_Last {
            get {
                object obj = ResourceManager.GetObject("pulsante_02_architetto_f_01_Last", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
    }
}
