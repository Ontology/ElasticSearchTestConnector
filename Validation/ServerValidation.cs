﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ElasticSearchTestConnector.Validation
{
    public class ServerValidation : ValidationRule
    {
        private string validIpAddressRegex = @"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
        private string validHostnameRegex = @"^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$";

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            var regexIpAddress = new Regex(validIpAddressRegex);
            var regexHostname = new Regex(validHostnameRegex);

            if (regexIpAddress.Match(value.ToString()).Success || regexHostname.Match(value.ToString()).Success)
            {
                return new ValidationResult(true, null);
            }
            else
            {
                return new ValidationResult(false, "Please enter a valid Servername.");
            }

            
        }
        
    }
}
