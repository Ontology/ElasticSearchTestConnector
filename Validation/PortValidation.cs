﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ElasticSearchTestConnector.Validation
{
    public class PortValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {

            int port;
            if (int.TryParse(value.ToString(), out port))
            {
                if (port > 9000 && port < 65536)
                {
                    return new ValidationResult(true, null);
                }
                else
                {
                    return new ValidationResult(false, "Please enter a valid Port.");
                }
                
            }
            else
            {
                return new ValidationResult(false, "Please enter a valid Port.");
            }
            

        }

    }
}
