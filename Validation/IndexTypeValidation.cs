﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ElasticSearchTestConnector.Validation
{
    public class IndexTypeValidation : ValidationRule
    {
        private string validIndexName = @"^([a-zA-Z_\S])+";
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {

            var regexIndexName = new Regex(validIndexName);
            
            if (regexIndexName.Match(value.ToString()).Success)
            {
                
                return new ValidationResult(true, null);
                
            }
            else
            {
                return new ValidationResult(false, "Please enter a valid Index.");
            }


        }

    }
}
