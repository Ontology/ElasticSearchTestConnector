﻿using ElasticSearchTestConnector.BaseClasses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using ElasticsearchConnector;

namespace ElasticSearchTestConnector
{
    public class MainViewModel : ViewModelBase
    {

        private EsSelector esSelector;

        private double opacityButtonFirst = 0.8;
        public double OpacityButtonFirst
        {
            get { return opacityButtonFirst; }
            set
            {
                opacityButtonFirst = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_OpacityButtonFirst);
            }
        }

        private double opacityButtonPrevious = 0.8;
        public double OpacityButtonPrevious
        {
            get { return opacityButtonPrevious; }
            set
            {
                opacityButtonPrevious = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_OpacityButtonPrevious);
            }
        }

        private double opacityButtonNext = 0.8;
        public double OpacityButtonNext
        {
            get { return opacityButtonNext; }
            set
            {
                opacityButtonNext = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_OpacityButtonNext);
            }
        }

        private double opacityButtonLast = 0.8;
        public double OpacityButtonLast
        {
            get { return opacityButtonLast; }
            set
            {
                opacityButtonLast = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_OpacityButtonLast);
            }
        }

        private bool enabledButtonOrderFirst = false;
        public bool EnabledButtonOrderFirst
        {
            get { return enabledButtonOrderFirst; }
            set
            {
                enabledButtonOrderFirst = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_EnabledButtonOrderFirst);
            }
        }

        private bool enabledButtonOrderPrevious = false;
        public bool EnabledButtonOrderPrevious
        {
            get { return enabledButtonOrderPrevious; }
            set
            {
                enabledButtonOrderPrevious = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_EnabledButtonOrderPrevious);
            }
        }

        private bool enabledButtonOrderNext = false;
        public bool EnabledButtonOrderNext
        {
            get { return enabledButtonOrderNext; }
            set
            {
                enabledButtonOrderNext = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_EnabledButtonOrderNext);
            }
        }

        private bool enabledButtonOrderLast = false;
        public bool EnabledButtonOrderLast
        {
            get { return enabledButtonOrderLast; }
            set
            {
                enabledButtonOrderLast = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_EnabledButtonOrderLast);
            }
        }

        public List<object> VisibleRowItems { get; set; }


        private ObservableCollection<object> itemList;
        public ObservableCollection<object> ItemList
        {
            get { return itemList; }
            set
            {
                itemList = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_ItemList);
            }
        }

        private string label_Server;
        public string Label_Server
        {
            get { return label_Server; }
            set
            {
                label_Server = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_LabelServer);
            }
        }

        private string label_Port;
        public string Label_Port
        {
            get { return label_Port; }
            set
            {
                label_Port = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_LabelPort);
            }
        }

        private string label_Index;
        public string Label_Index
        {
            get { return label_Index; }
            set
            {
                label_Index = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_LabelIndex);
            }
        }

        private string label_Type;
        public string Label_Type
        {
            get { return label_Type; }
            set
            {
                label_Type = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_LabelType);
            }
        }

        private string label_Query;
        public string Label_Query
        {
            get { return label_Query; }
            set
            {
                label_Query = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_LabelQuery);
            }
        }

        private string server;
        public string Server
        {
            get { return server; }
            set
            {
                server = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_Server);
            }
        }

        private int port;
        public int Port
        {
            get { return port; }
            set
            {
                port = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_Port);
            }
        }

        private string index;
        public string Index
        {
            get { return index; }
            set
            {
                index = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_Index);
            }
        }

        private string type;
        public string Type
        {
            get { return type; }
            set
            {
                type = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_Type);
            }
        }

        private string query;
        public string Query
        {
            get { return query; }
            set
            {
                query = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_Query);
            }
        }

        private string button_Query;
        public string Button_Query
        {
            get { return button_Query; }
            set
            {
                button_Query = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_ButtonQuery);
            }
        }

        private int page;
        public int Page
        {
            get { return page; }
            set
            {
                page = value;
                Label_Navigation = page + "\\" + pageCount;
                RaisePropertyChanged(Properties.Resources.NotifyChange_Page);
            }
        }

        private int pageCount;
        public int PageCount
        {
            get { return pageCount; }
            set
            {
                pageCount = value;
                Label_Navigation = page + "\\" + pageCount;
                RaisePropertyChanged(Properties.Resources.NotifyChange_PageCount);
            }
        }

        private string label_Navigation;
        public string Label_Navigation
        {
            get { return label_Navigation; }
            set
            {
                label_Navigation = value;
                RaisePropertyChanged(Properties.Resources.NotifyChange_LabelNavigation);
            }
        }


        public void RequestNextPage()
        {
            RequestQuery(esSelector.LastPos);

            
        }

        public void RequestPreviousPage()
        {
            RequestQuery(esSelector.CurPage * esSelector.SearchRange - esSelector.SearchRange);
            

        }

        public void RequestFirstPage()
        {
            RequestQuery(0);

        }

        public void RequestLastPage()
        {
            RequestQuery((int)esSelector.Total - esSelector.SearchRange);

        }

        private void RequestQuery(int pos)
        {
            esSelector = new EsSelector(Server, Port, Index, 20000, Guid.NewGuid().ToString());
            var documents = esSelector.GetData_Documents(Type, Index.ToLower(), true, pos, Query);

            ItemList = new ObservableCollection<object>(DynamicItemBuilder.CreateNewObjectList(Type, esSelector.FieldItems, documents));

            ConfigurePageAndCount();
            ConfigureButtons();

            ConfigurePageAndCount();
            ConfigureButtons();
        }

        public void ConfigurePageAndCount()
        {
            Page = esSelector.CurPage+1;
            PageCount = esSelector.PageCount-1;
        }

        public void ConfigureButtons()
        {
            EnabledButtonOrderFirst = Page > 1;
            EnabledButtonOrderPrevious = Page > 1;
            EnabledButtonOrderNext = Page < PageCount;
            EnabledButtonOrderLast = Page < PageCount;

            OpacityButtonFirst = Page > 1 ? 1 : 0.8;
            OpacityButtonPrevious = Page > 1 ? 1 : 0.8;
            OpacityButtonNext = Page < PageCount ? 1 : 0.8;
            OpacityButtonLast = Page < PageCount ? 1 : 0.8;
        }

        public MainViewModel()
        {
            Label_Server = "Server:";
            Label_Index = "Index:";
            Label_Port = "Port:";
            Label_Type = "Type:";
            Label_Query = "Query:";
            Button_Query = "Query";

            Page = 0;
            PageCount = 0;
        }
    }
}
