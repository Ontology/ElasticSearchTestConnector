﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ElasticSearchTestConnector
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainViewModel viewModel;
        public MainWindow()
        {
            InitializeComponent();
            viewModel = (MainViewModel)DataContext;
        }

        private void Button_Click_First(object sender, RoutedEventArgs e)
        {
            viewModel.RequestFirstPage();
        }

        private void Button_Click_Previous(object sender, RoutedEventArgs e)
        {
            viewModel.RequestPreviousPage();
        }

        private void Button_Click_Next(object sender, RoutedEventArgs e)
        {
            viewModel.RequestNextPage();
        }

        private void Button_Click_Last(object sender, RoutedEventArgs e)
        {
            viewModel.RequestLastPage();
        }
    }
}
